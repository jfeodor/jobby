#!/usr/bin/env bash

python -m grpc.tools.protoc -I src/jobby --grpc_python_out=src --python_out=src jobby.proto
