import jobby_pb2
import jobby_pb2_grpc

from .db import VolatileDatabase


class ProjectsServicer(jobby_pb2_grpc.JobbyProjectsServicer):

    def __init__(self, db: VolatileDatabase):
        self.db = db

    def CreateProject(self, request, context):
        project_id = self.db.create_project(request.name)
        return jobby_pb2.CreateProjectResponse(project_id=project_id)

    def ListProjects(self, request, context):
        # TODO: Use the query to filter projects by name
        projects = [jobby_pb2.Project(project_id=project_id, name=project["name"])
                    for project_id, project in self.db.list_projects()]
        return jobby_pb2.ListProjectsResponse(projects=projects)
