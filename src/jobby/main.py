import grpc
import os
from concurrent.futures import ThreadPoolExecutor

import jobby_pb2_grpc
from . import jobs
from . import projects
from .db import VolatileDatabase
from .executions import ExecutionsService


def main():
    db = VolatileDatabase()
    executions = ExecutionsService(os.environ["K8S_CONF_PATH"], os.environ["K8S_HOST"])
    server = grpc.server(ThreadPoolExecutor())
    jobby_pb2_grpc.add_JobbyJobsServicer_to_server(jobs.JobsServicer(db, executions), server)
    jobby_pb2_grpc.add_JobbyProjectsServicer_to_server(projects.ProjectsServicer(db), server)
    server.add_insecure_port("0.0.0.0:50000")
    server.start()
    server.wait_for_termination()


if __name__ == "__main__":
    main()
