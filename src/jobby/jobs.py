import jobby_pb2
import jobby_pb2_grpc
from .db import VolatileDatabase
from .executions import ExecutionsService

class JobsServicer(jobby_pb2_grpc.JobbyJobsServicer):

    def __init__(self, db: VolatileDatabase, executions: ExecutionsService):
        self.db = db
        self.executions = executions

    def CreateJob(self, request, context):
        variables = [{"name": var.name, "default": var.default, "required": var.required} for var in request.variables]
        job_id = self.db.create_job(request.project_id, request.name, request.image, variables)
        return jobby_pb2.CreateJobResponse(job_id=job_id)

    def ListJobs(self, request, context):
        jobs = []
        for job_id, job_data in self.db.list_jobs(request.project_id):
            variables = [jobby_pb2.JobEnvironmentVariable(name=var["name"], default=var["default"], required=var["required"]) for var in job_data["variables"]]
            job = jobby_pb2.Job(job_id=job_id, project_id=job_data["project_id"], name=job_data["name"], image=job_data["image"], variables=variables)
            jobs.append(job)
        return jobby_pb2.ListJobsResponse(jobs=jobs)

    def CreateExecution(self, request, context):
        # TODO: Validate environment variables
        #  * Required job environment variables should be present
        #  * Default values from job environment variables should be used
        job = self.db.get_job(request.job_id)
        execution_id = self.executions.create_execution(request.job_id, job["image"],
                                                        {var.name: var.value for var in request.variables})
        return jobby_pb2.CreateExecutionResponse(execution_id=execution_id)

    def GetExecution(self, request, context):
        status, logs = self.executions.get_execution(request.execution_id)
        return jobby_pb2.GetExecutionResponse(status=status, logs=logs)
