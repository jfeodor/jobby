import uuid

from kubernetes import client


class ExecutionsService:

    def __init__(self, conf_path, k8s_host):
        api_configuration = client.Configuration()
        api_configuration.host = k8s_host
        api_configuration.ssl_ca_cert = f"{conf_path}/ca.crt"
        api_configuration.cert_file = f"{conf_path}/admin.crt"
        api_configuration.key_file = f"{conf_path}/admin.key"
        self.api_client = client.ApiClient(api_configuration)
        self.core = client.CoreV1Api(self.api_client)
        self.namespace = "default"

    def create_execution(self, job_id, image: str, variables: dict[str, str]):
        execution_id = str(uuid.uuid4()).replace('_', '')
        pod = client.V1Pod(
            api_version="v1",
            kind="Pod",
            metadata=client.V1ObjectMeta(
                namespace=self.namespace,
                name=f"{execution_id}",
                labels={"job_id": job_id}
            ),
            spec=client.V1PodSpec(
                restart_policy="Never",
                containers=[
                    client.V1Container(
                        name="container",
                        image=image,
                        command=[],
                        env=[client.V1EnvVar(name=name, value=value) for name, value in variables.items()]
                    )
                ]
            )
        )
        self.core.create_namespaced_pod(self.namespace, pod)
        return execution_id

    def get_execution(self, execution_id: str):
        pod = self.core.read_namespaced_pod(execution_id, self.namespace)
        pod_log = None
        try:
            pod_log = self.core.read_namespaced_pod_log(execution_id, self.namespace).splitlines()
        except client.ApiException:
            pass
        return (pod.status.phase, pod_log)
