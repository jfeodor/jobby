import uuid
from typing import Optional, TypedDict


class JobEnvironmentVariable(TypedDict):
    name: str
    default: Optional[str]
    required: bool


class VolatileDatabase:

    def __init__(self):
        self._jobs: dict[str, dict] = {}
        self._project_jobs: dict[str, set[str]] = {}
        self._projects: dict[str, dict] = {}

    def create_job(self, project_id: str, name: str, image: str, variables: list[JobEnvironmentVariable]):
        job_id = str(uuid.uuid4())
        self._project_jobs[project_id].add(job_id)
        self._jobs[job_id] = {"project_id": project_id, "name": name, "image": image, "variables": variables}
        return job_id

    def get_job(self, job_id: str):
        return self._jobs[job_id]

    def list_jobs(self, project_id: str):
        return [(job_id, self.get_job(job_id)) for job_id in self._project_jobs[project_id]]

    def create_project(self, name: str):
        project_id = str(uuid.uuid4())
        self._projects[project_id] = {"name": name, "jobs": []}
        self._project_jobs[project_id] = set()
        return project_id

    def get_project(self, project_id: str):
        return self._projects[project_id]

    def list_projects(self):
        return self._projects.items()
