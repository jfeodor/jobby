import pytest

from jobby_pb2_grpc import JobbyJobsStub, JobbyProjectsStub
from jobby_pb2 import CreateJobRequest, CreateProjectRequest, JobEnvironmentVariable, ListJobsRequest


@pytest.fixture()
def project_id(projects_client: JobbyProjectsStub):
    return projects_client.CreateProject(CreateProjectRequest(name='DefaultProject')).project_id


def test_initially_no_jobs(jobs_client: JobbyJobsStub, project_id: str):
    response = jobs_client.ListJobs(ListJobsRequest(project_id=project_id))
    assert list(response.jobs) == []


def test_create_job(jobs_client: JobbyJobsStub, project_id: str):
    response = jobs_client.CreateJob(CreateJobRequest(project_id=project_id, name="TestJob", image="registry.com/image:tag",
                                                      variables=[JobEnvironmentVariable(name="ENV_VAR", required=True)]))
    listed_job = list(jobs_client.ListJobs(ListJobsRequest(project_id=project_id)).jobs)[0]
    assert listed_job.job_id == response.job_id
    assert listed_job.name == "TestJob"
    assert listed_job.image == "registry.com/image:tag"
    variables = list(listed_job.variables)
    assert variables[0].name == "ENV_VAR"
    assert variables[0].required == True
    assert variables[0].default == ""
