import concurrent.futures
import os
import grpc
import pytest
from jobby.executions import ExecutionsService

import jobby_pb2_grpc
from jobby.db import VolatileDatabase
from jobby.jobs import JobsServicer
from jobby.projects import ProjectsServicer


@pytest.fixture
def db():
    return VolatileDatabase()


@pytest.fixture
def executions():
    return ExecutionsService(os.environ["K8S_CONF_PATH"], os.environ["K8S_HOST"])


@pytest.fixture
def server(db, executions):
    server = grpc.server(concurrent.futures.ThreadPoolExecutor())
    jobby_pb2_grpc.add_JobbyJobsServicer_to_server(JobsServicer(db, executions), server)
    jobby_pb2_grpc.add_JobbyProjectsServicer_to_server(ProjectsServicer(db), server)
    server.add_insecure_port("0.0.0.0:50000")
    server.start()
    yield server
    server.stop(grace=1)


@pytest.fixture
def channel(server):
    channel = grpc.insecure_channel("localhost:50000")
    yield channel
    channel.close()


@pytest.fixture
def jobs_client(channel):
    return jobby_pb2_grpc.JobbyJobsStub(channel)


@pytest.fixture
def projects_client(channel):
    return jobby_pb2_grpc.JobbyProjectsStub(channel)
