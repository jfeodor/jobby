from jobby_pb2_grpc import JobbyProjectsStub
from jobby_pb2 import ListProjectsRequest, CreateProjectRequest


def test_initially_no_projects(projects_client: JobbyProjectsStub):
    response = projects_client.ListProjects(ListProjectsRequest())
    assert list(response.projects) == []


def test_create_project(projects_client: JobbyProjectsStub):
    project_id = projects_client.CreateProject(CreateProjectRequest(name='TestProject')).project_id
    response = projects_client.ListProjects(ListProjectsRequest())
    listed_project = list(response.projects)[0]
    assert listed_project.project_id == project_id
    assert listed_project.name == "TestProject"
