from time import sleep, time
import pytest

from jobby_pb2_grpc import JobbyJobsStub, JobbyProjectsStub
from jobby_pb2 import CreateExecutionRequest, CreateJobRequest, CreateProjectRequest, GetExecutionRequest, \
                      ExecutionEnvironmentVariable


@pytest.fixture
def project_id(projects_client: JobbyProjectsStub):
    return projects_client.CreateProject(CreateProjectRequest(name='DefaultProject')).project_id


@pytest.fixture
def job_id(jobs_client: JobbyJobsStub, project_id: str):
    return jobs_client.CreateJob(CreateJobRequest(project_id=project_id, name="TestJob",
                                                  image="host.docker.internal:5000/test_image:latest")).job_id


def test_execute_job(jobs_client: JobbyJobsStub, job_id: str):
    execution_id = jobs_client.CreateExecution(CreateExecutionRequest(job_id=job_id, variables=[ExecutionEnvironmentVariable(name="THIS", value="It is what I expected")])).execution_id

    executed_time = time()
    while True:
        assert time() - executed_time < 30
        response = jobs_client.GetExecution(GetExecutionRequest(execution_id=execution_id))
        if response.status == "Succeeded":
            break
        assert response.status in {"Pending", "Running"}
        sleep(1)
    assert response.logs == ['Running a test job', 'That is also a haiku', 'Because it is cool', 'And finally this: It is what I expected']
