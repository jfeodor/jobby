#!/usr/bin/env bash

echo "> Getting k0s credentials..."
mkdir -p k0s
docker-compose cp k0s:/var/lib/k0s/pki/ca.crt k0s/ca.crt
docker-compose cp k0s:/var/lib/k0s/pki/admin.crt k0s/admin.crt
docker-compose cp k0s:/var/lib/k0s/pki/admin.key k0s/admin.key

echo "> Building and pushing test image to local registry"
docker build -t localhost:5000/test_image:latest -f images/test_image/Dockerfile images/test_image/
docker push localhost:5000/test_image:latest

echo "> Running tests"
PYTHONPATH=$PWD/src K8S_CONF_PATH=k0s K8S_HOST=https://localhost:6443 python -m pytest tests
