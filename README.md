# Jobby - the interview exercise sample project

This is a small sample project that implements a gRPC web service that can manage projects and jobs, launch
executions of jobs and retrieve the logs of the executions.

Have a look at the TODO and try to implement them.

## TODOs

 * Ability to query projects by name
 * Validation of environment variables passed to executions
 * Ability for executions to store a return value


## Generate python code from protobuf

This project is based on gRPC which uses protocol buffers to define the API, and also to generate base code. Run the
`generate.sh` script to update the `jobby_pb2` and `jobby_pb2_grpc` modules with changes to the `jobby.proto` file.

    ./generate.sh


## Running the service

    docker-compose build jobby
    docker-compose up jobby


## Running the test suite

The script `run_tests.sh` performs the necessary actions to run the test suite, including copying credentials needed
for accessing the k0s cluster, build and push the test image to the registry, and run the test suite itself.

It is a prerequisite that the `k0s` service is running already:

    docker-compose up -d k0s


## Monitoring k0s

You can run `kubectl` from within the `k0s` container. For example to see if the node is ready

    docker-compose exec k0s kubectl get node

Or to list the pods

    docker-compose exec k0s kubectl get pod

Or getting the logs of a specific pod

    docker-compose exec k0s kubectl logs <POD NAME>


## Troubleshooting k0s

Sometimes k0s can make trouble. It can be a good idea to

    docker-compose down

And bring it up again, with fresh volumes etc

    docker-compose up -d --force-recreate -V k0s
